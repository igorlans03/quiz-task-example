import {SelectOption} from "@/types/common.ts";
import {FC, useState} from "react";
import SelectButton from "@/components/common/ui/select-button/select-button.tsx";
import styles from './multiple-select.module.scss';

type MultipleSelectProps = {
    value?: string[];
    onValueChange?: (newValue: string[]) => void;
    defaultValue?: string[];
    options: SelectOption[];
}

const MultipleSelect: FC<MultipleSelectProps> = ({value: outerValues, onValueChange, options, defaultValue}) => {

    const [innerValues, setInnerValues] = useState(defaultValue || []);

    const values = outerValues === undefined ? innerValues : outerValues;
    const setValues = (newValue: string[]) => {
        setInnerValues(newValue)
        onValueChange?.(newValue)
    }
    const handleItemToggle = (newValue: string) => {
        const isInValues = values.some(item => item === newValue);

        let newValues;

        if (isInValues) {
            newValues = values.filter(item => item !== newValue);
        } else {
            newValues = [...values, newValue]
        }

        setValues(newValues);
    }




    return (
        <div className={styles.multipleSelect}>
            {options.map(option =>
                <SelectButton value={values.some(item => item === option.value)} key={option.value} onClick={() => handleItemToggle(option.value)}>
                    <SelectButton.Content>
                        {option.label}
                    </SelectButton.Content>
                    <SelectButton.Checkbox />
                </SelectButton>
            )}
        </div>
    );
};

export default MultipleSelect;