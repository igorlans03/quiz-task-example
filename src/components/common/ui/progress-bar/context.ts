import {createContext, useContext} from "react";

type ProgressBarContext = {
    value: number;
    maxLength: number;
}

export const ProgressBarContext = createContext<ProgressBarContext | null>(null)

export const useProgressBar = () => {
    const context = useContext(ProgressBarContext);

    if (!context) {
        throw new Error('This component must be used within a <ProgressBar> component.');
    }

    return context;
}