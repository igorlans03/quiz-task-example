import {FC, useMemo} from "react";
import {WrapperDivProps} from "@/types/common.ts";
import {Counter} from "@/components/common/ui/progress-bar/modules/counter/counter.tsx";
import {Line} from "@/components/common/ui/progress-bar/modules/line/line.tsx";
import {ProgressBarContext} from "@/components/common/ui/progress-bar/context.ts";
import styles from './progress-bar.module.scss';

type ProgressBarProps = {
    value: number;
    maxLength: number;
} & WrapperDivProps

type ProgressBarComposition = {
    Line: typeof Line;
    Counter: typeof Counter
}


const ProgressBar: FC<ProgressBarProps> & ProgressBarComposition = ({value, maxLength, children, ...props}) => {


    const memoizedContextValue = useMemo(
        () => ({
            value,
            maxLength
        }),
        [value, maxLength],
    );


    return (
        <ProgressBarContext.Provider value={memoizedContextValue}>
            <div className={styles.progressBar} {...props}>
                {children}
            </div>
        </ProgressBarContext.Provider>
    );
};



ProgressBar.Counter = Counter;
ProgressBar.Line = Line;

export default ProgressBar;