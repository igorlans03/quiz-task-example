import styles from './line.module.scss';
import {useProgressBar} from "../../context.ts";
import {FC, useMemo} from "react";

type LineProps = {
    percent?: number;
}

export const Line: FC<LineProps> = ({percent: outerPercent}) => {
    const {value, maxLength} = useProgressBar();
    const calculatedPercent = useMemo(() => (100 / maxLength) * value, [maxLength, value]);

    const roundedPercent = calculatedPercent.toFixed();
    const roundedOuterPercent = outerPercent?.toFixed();

    const percent = outerPercent ? roundedOuterPercent as string : roundedPercent;

    return (
        <div className={styles.line}>
            <div className={styles.line__filled} style={{width: `${percent}%`}}></div>
        </div>
    )
}