import styles from './counter.module.scss';
import {useProgressBar} from "../../context.ts";
import Highlighted from "@/components/common/highlighted/highlighted.tsx";

export const Counter = () => {
    const {value, maxLength} = useProgressBar();

    return (
        <div className={styles.counter}>
            <Highlighted>{value.toFixed()}</Highlighted> / {maxLength.toFixed()}
        </div>
    )
}