import {ChangeEvent, ComponentPropsWithRef, FC, useState} from "react";
import classNames from "classnames";
import styles from './input.module.scss';


type InputProps = ComponentPropsWithRef<'input'> & {
    value?: string;
    onValueChange?: (newValue: string) => void;
    defaultValue?: string
}
const Input: FC<InputProps> = ({value, onValueChange, defaultValue = "", className: outerClassname, ...props}) => {

    const [input, setInput] = useState(defaultValue);

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value;
        setInput(newValue);
        onValueChange?.(newValue);
    }

    const inputValue = value === undefined ? input : value;

    const className = classNames(styles.input, outerClassname)


    return <input className={className} value={inputValue} onChange={handleChange} type={'text'} {...props}/>

};

export default Input;