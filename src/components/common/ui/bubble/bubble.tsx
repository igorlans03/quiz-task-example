import {FC, useState} from "react";
import {CheckboxProps} from "@/components/common/ui/checkbox/checkbox.tsx";
import classNames from "classnames";
import {WrapperDivProps} from "@/types/common.ts";
import styles from './bubble.module.scss';

type BubbleProps = CheckboxProps & WrapperDivProps;

const Bubble: FC<BubbleProps> = ({
                                     children,
                                     className,
                                     value,
                                     onValueChange,
                                     defaultValue = false,
                                     ...props
                                 }) => {

    const [checked, setChecked] = useState(defaultValue);

    const handleChange = (newValue: boolean) => {
        setChecked(newValue);
        onValueChange?.(newValue);
    }

    const checkedValue = value === undefined ? checked : value;

    const rootClassName = classNames(
        styles.bubble,
        {
            [styles.bubble_active]: checkedValue
        },
        className
    );


    return (
        <div className={rootClassName} onClick={() => handleChange(!checkedValue)} {...props}>
            <div className={styles.bubble__border}>
            </div>

            <div className={styles.bubble__content}>
                {children}
            </div>
        </div>
    );
};

export default Bubble;