import {ComponentPropsWithRef, FC} from "react";
import styles from './button.module.scss';

export type ButtonProps = {
    text: string
} & ComponentPropsWithRef<'button'>;
const Button: FC<ButtonProps> = ({text, className, ...props }) => {
    const classNames = [styles.button, className].join(' ');

    return (
        <button className={classNames} {...props}>
            {text}
        </button>
    );
};

export default Button;