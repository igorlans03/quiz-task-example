import {createContext, useContext} from "react";

type SelectButtonContextProps = {
    checked: boolean;
    handleChange: (newValue: boolean) => void;
    defaultValue?: boolean
}

export const SelectButtonContext = createContext<SelectButtonContextProps | null>(null);


export const useSelectButton = () => {
    const context = useContext(SelectButtonContext);

    if (!context) {
        throw new Error('This component must be used within a <SelectButton> component.');
    }

    return context;
}