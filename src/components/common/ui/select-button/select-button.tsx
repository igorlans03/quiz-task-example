import {FC, useMemo, useState} from "react";
import {CheckboxProps} from "@/components/common/ui/checkbox/checkbox.tsx";
import styles from './select-button.module.scss';
import classNames from "classnames";
import {WrapperDivProps} from "@/types/common.ts";
import Content from "@/components/common/ui/select-button/modules/content/content.tsx";
import {SelectButtonContext} from "@/components/common/ui/select-button/context.ts";
import SelectButtonCheckbox from "@/components/common/ui/select-button/modules/checkbox/checkbox.tsx";

type SelectButtonProps = CheckboxProps & WrapperDivProps;

type SelectButtonComposition = {
    Content: typeof Content;
    Checkbox: typeof SelectButtonCheckbox
}
const SelectButton: FC<SelectButtonProps> & SelectButtonComposition = ({
                                                                           children,
                                                                           className,
                                                                           value,
                                                                           onValueChange,
                                                                           defaultValue = false,
                                                                           ...props
                                                                       }) => {

    const [checked, setChecked] = useState(defaultValue);

    const handleChange = (newValue: boolean) => {
        setChecked(newValue);
        onValueChange?.(newValue);
    }

    const checkedValue = value === undefined ? checked : value;

    const rootClassName = classNames(
        styles.selectButton,
        {
            [styles.selectButton_active]: checkedValue
        },
        className
    );


    const memoizedContextValue = useMemo(
        () => ({
            checked: checkedValue,
            handleChange,
            defaultValue
        }),
        [checkedValue, handleChange, defaultValue],
    );

    return (
        <SelectButtonContext.Provider value={memoizedContextValue}>
            <div className={rootClassName} onClick={() => handleChange(!checkedValue)} {...props}>
                <div className={styles.backdrop}>
                </div>

                <div className={styles.selectButton__content}>
                    {children}
                </div>
            </div>
        </SelectButtonContext.Provider>


    );
};


SelectButton.Checkbox = SelectButtonCheckbox;
SelectButton.Content = Content;
export default SelectButton;