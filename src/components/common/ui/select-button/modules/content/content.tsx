import {FC} from "react";
import {WrapperDivProps} from "@/types/common.ts";


type ContentProps = WrapperDivProps
const Content: FC<ContentProps> = ({children, className, ...props}) => {

    return (
        <div {...props}>
            {children}
        </div>
    );
};

export default Content;