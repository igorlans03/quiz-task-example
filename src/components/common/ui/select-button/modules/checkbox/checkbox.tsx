import {useSelectButton} from "@/components/common/ui/select-button/context.ts";
import Checkbox from "@/components/common/ui/checkbox/checkbox.tsx";


const SelectButtonCheckbox = () => {

    const {checked, handleChange, defaultValue} = useSelectButton();

    return  <Checkbox value={checked} onValueChange={handleChange} defaultValue={defaultValue} />

};

export default SelectButtonCheckbox;