import React, { useEffect, useState } from 'react';
import variables from '@/styles/variables.module.scss';

interface ProgressCircleProps {
    timeout: number;
    onComplete: () => void;
}

const ProgressCircle: React.FC<ProgressCircleProps> = ({ timeout, onComplete }) => {
    const [progress, setProgress] = useState<number>(0);

    useEffect(() => {
        let interval: NodeJS.Timeout;
        if (progress < 100) {
            interval = setInterval(() => {
                setProgress((prevProgress) => Math.min(prevProgress + 1, 100));
            }, (timeout * 1000) / 100); // Adjust the division factor to control the speed
        } else {
            onComplete();
        }

        return () => clearInterval(interval);
    }, [progress, timeout, onComplete]);

    const radius: number = 125;
    const circumference: number = 2 * Math.PI * radius;
    const progressOffset: number = ((100 - progress) / 100) * circumference;

    return (
        <div style={{ width: '250px', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <svg
                viewBox="-5 -5 260 260" // Adjusted viewBox for a bit of padding
                height="100%"
                width="100%"
            >
                <circle
                    r={radius}
                    cx={radius}
                    cy={radius}
                    fill="transparent"
                    stroke="#ccc"
                    strokeWidth={10}
                />
                <circle
                    r={radius}
                    cx={radius}
                    cy={radius}
                    fill="transparent"
                    stroke={variables.primaryColor}
                    strokeWidth={10}
                    strokeDasharray={circumference}
                    strokeDashoffset={progressOffset}
                    strokeLinecap="round"
                />
                <text x="50%" y="50%" textAnchor="middle" dy="0.3em" fontSize={52} fontWeight={800} fill="#fff">
                    {progress}%
                </text>
            </svg>
        </div>
    );
};

export default ProgressCircle;
