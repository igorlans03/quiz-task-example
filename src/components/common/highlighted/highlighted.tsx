import {FC} from "react";
import styles from './highlighted.module.scss';
import {WrapperDivProps} from '@/types/common.ts';
import SCSSVariables from '@/styles/variables.module.scss';

type HighlightedProps = WrapperDivProps & {
    color?: string
};
const Highlighted: FC<HighlightedProps> = ({
                                               children,
                                               className,
                                               color= SCSSVariables.primaryColor,
                                               style,
                                               ...props
                                           }) => {
    const classNames = [styles.highlighted, className].join(' ');

    return (
        <span className={classNames} style={{color, ...style}} {...props}>
            {children}
        </span>
    );
};

export default Highlighted;