import {SelectOption} from "@/types/common.ts";
import {FC, useState} from "react";
import styles from './single-select.module.scss';
import SelectButton from "@/components/common/ui/select-button/select-button.tsx";

type SingleSelectProps = {
    value?: string;
    onValueChange?: (newValue: string) => void;
    defaultValue?: string;
    options: SelectOption[];
}

const SingleSelect:FC<SingleSelectProps> = ({value: outerValue, onValueChange, options, defaultValue}) => {

    const [innerValue, setInnerValue] = useState(defaultValue ||  null);

    const handleItemClick = (newValue: string) => {
        setInnerValue(newValue);
        onValueChange?.(newValue);
    }

    const value = outerValue === undefined ? innerValue : outerValue;

    return (
        <div className={styles.singleSelect}>
            {options.map(item =>
                <SelectButton value={value === item.value} key={item.value} onClick={() => handleItemClick(item.value)}>
                    <SelectButton.Content>
                        {item.label}
                    </SelectButton.Content>
                </SelectButton>
            )}
        </div>
    );
};

export default SingleSelect;