import {SelectOption} from "@/types/common.ts";
import {FC, useState} from "react";
import Bubble from "@/components/common/ui/bubble/bubble.tsx";
import styles from './bubble-multiple-select.module.scss';

type BubbleMultipleSelectProps = {
    value?: string[];
    onValueChange?: (newValue: string[]) => void;
    defaultValue?: string[];
    options: SelectOption[];
}

const BubbleMultipleSelect: FC<BubbleMultipleSelectProps> = ({
                                                                 value: outerValues,
                                                                 onValueChange,
                                                                 options,
                                                                 defaultValue
                                                             }) => {

    const [innerValues, setInnerValues] = useState(defaultValue || []);

    const values = outerValues === undefined ? innerValues : outerValues;

    const setValues = (newValue: string[]) => {
        setInnerValues(newValue)
        onValueChange?.(newValue)
    }
    const handleItemToggle = (newValue: string) => {
        const isInValues = values.some(item => item === newValue);

        let newValues;

        if (isInValues) {
            newValues = values.filter(item => item !== newValue);
        } else {
            newValues = [...values, newValue]
        }

        setValues(newValues);
    }


    return (
        <div className={styles.bubbleMultipleSelect}>
            {options.map(option =>
                <Bubble value={values.some(item => item === option.value)} key={option.value}
                        onClick={() => handleItemToggle(option.value)}>
                    {option.label}
                </Bubble>
            )}
        </div>
    );
};

export default BubbleMultipleSelect;