import Papa from 'papaparse';
import {Locale, QuestionWithAnswer} from "@/types/quiz.ts";


const convertAnswers = (questions: QuestionWithAnswer[], lang: Locale = 'en') => {
    return {
        fields: ['Order', 'Title', 'Type', "Answer"],
        data: questions.map((question: any, index) => {
            const translatedTitle = question?.title?.[lang];

            const title = translatedTitle || question.title;
            return [index + 1, title, question.type, question?.answer?.toString()]
        })
    }
}
const generateCsv = (data: any, fileName: string = 'quiz-results') => {
    const csv = Papa.unparse(convertAnswers(data));
    const blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });

    const link = document.createElement('a');
    if (link.download !== undefined) {
        const url = URL.createObjectURL(blob);
        link.setAttribute('href', url);
        link.setAttribute('download', fileName);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    } else {
        alert('Your browser does not support the download attribute.');
    }
};




export default generateCsv;