import styles from './header.module.scss';
import QuizProgressBar from "@/components/quiz/modules/header/progress-bar/progress-bar.tsx";
import BackButton from "@/components/quiz/modules/header/back-button/back-button.tsx";

const Header = () => {
    return (
        <div className={styles.header}>
            <BackButton />
            <QuizProgressBar />

        </div>
    );
};

export default Header;