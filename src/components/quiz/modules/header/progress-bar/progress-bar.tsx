import ProgressBar from "@/components/common/ui/progress-bar/progress-bar.tsx";
import {useQuizContext} from "@/components/quiz/context.ts";

const QuizProgressBar = () => {
    const {questionsCount, questionPosition, isLast} = useQuizContext();

    const value = isLast ? questionPosition + 0.5 : questionPosition + 1;

    if (!questionsCount) return;

    return (
       <ProgressBar value={value} maxLength={questionsCount}>
           <ProgressBar.Counter />
           <ProgressBar.Line />
       </ProgressBar>
    );
};

export default QuizProgressBar;