import styles from './form.module.scss';
import {WrapperDivProps} from "@/types/common.ts";
import {FC} from "react";
import classNames from "classnames";

type FormProps = WrapperDivProps & {

}
const Form: FC<FormProps> = ({children, className, ...props}) => {

    const rootClassName = classNames(styles.form, className);

    return (
        <div className={rootClassName} {...props}>
            {children}
        </div>
    );
};

export default Form;