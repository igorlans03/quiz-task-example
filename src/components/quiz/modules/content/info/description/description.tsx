import {FC} from "react";
import {WrapperDivProps} from "@/types/common.ts";
import styles from './description.module.scss'


type DescriptionProps = WrapperDivProps;
const Description: FC<DescriptionProps> = ({children, className, ...props}) => {
    const classNames = [styles.description, className].join(' ')

    return (
        <div className={classNames} {...props}>
            {children}
        </div>
    );
};

export default Description;