import {FC} from "react";
import styles from './title.module.scss';
import {WrapperDivProps} from "@/types/common.ts";
import classNames from "classnames";

type TitleProps = WrapperDivProps;
const Title: FC<TitleProps> = ({children, className, ...props}) => {

    const rootClassName = classNames(styles.title, className)

    return (
        <div className={rootClassName} {...props}>
            {children}
        </div>
    );
};

export default Title;