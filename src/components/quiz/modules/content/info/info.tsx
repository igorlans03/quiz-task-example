import styles from './info.module.scss';
import {WrapperDivProps} from "@/types/common.ts";
import {FC} from "react";
import classNames from "classnames";

type InfoProps = WrapperDivProps & {

}
const Info: FC<InfoProps> = ({children, className, ...props}) => {

    const rootClassName = classNames(styles.info, className);

    return (
        <div className={rootClassName} {...props}>
            {children}
        </div>
    );
};

export default Info;