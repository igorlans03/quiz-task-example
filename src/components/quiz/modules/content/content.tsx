import {WrapperDivProps} from "@/types/common.ts";
import {FC} from "react";
import classNames from "classnames";
import styles from './content.module.scss';


type ContentProps = WrapperDivProps & {

}

const Content: FC<ContentProps> = ({children, className, ...props}) => {

    const rootClassName = classNames(styles.content, className);

    return (
        <div className={rootClassName} {...props}>
            {children}
        </div>
    );
};

export default Content;