import {createContext, useContext} from "react";
import useQuiz from "@/components/quiz/hooks/useQuiz.ts";



export const QuizContext = createContext<ReturnType<typeof useQuiz> | null>(null)

export const useQuizContext = () => {
    const context = useContext(QuizContext);

    if (!context) {
        throw new Error('This component must be used within a <Quiz> component.');
    }

    return context;
}