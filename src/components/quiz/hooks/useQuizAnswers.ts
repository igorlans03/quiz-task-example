import {useEffect, useMemo, useState} from "react";
import {QuestionWithAnswer} from "@/types/quiz.ts";
import {useParams} from "react-router-dom";

const useQuizAnswers = () => {
    const [answers, setAnswers] = useState<QuestionWithAnswer[] | null>(null);



    useEffect(() => {
        const lsQuestionsWithAnswers = window.localStorage.getItem('questionsWithAnswers');
        if (!lsQuestionsWithAnswers) return;

        const parsedQuestions = JSON.parse(lsQuestionsWithAnswers) as QuestionWithAnswer[];

        setAnswers(parsedQuestions);

    }, []);

    const handleSetAnswer = (questionWithAnswer: QuestionWithAnswer) => {
        const answersArr = answers || [];

        const isAnswerIsRepeated = answersArr.some(item => item.id === questionWithAnswer.id);

        const answersToSet = isAnswerIsRepeated ? answersArr.map(item => {
            if (item.id === questionWithAnswer.id) {
                return questionWithAnswer
            }

            return item;

        }) : [...answersArr, questionWithAnswer]


        setAnswers(answersToSet as QuestionWithAnswer[]);
        window.localStorage.setItem('questionsWithAnswers', JSON.stringify(answersToSet))
    };

    const handleClearAnswers = () => {
        window.localStorage.clear();
        setAnswers(null);
    }

    const params = useParams();

    const persistedCurrentQuestion = useMemo(() =>{
        const questionId = params.questionId;
        return answers?.find(item => item.id === questionId) as QuestionWithAnswer | undefined;
    }, [params, answers])

    return {
        answers,
        persistedCurrentQuestion,
        setAnswer: handleSetAnswer,
        clearAnswers: handleClearAnswers
    }
};

export default useQuizAnswers;