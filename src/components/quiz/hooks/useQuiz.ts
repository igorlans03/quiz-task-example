import {useEffect, useState} from "react";
import {Question} from "@/types/quiz.ts";
import useLocale from "@/components/quiz/hooks/useLocale.ts";
import {useParams} from "react-router-dom";
import db from '@/components/quiz/data/db.json';
import useQuizAnswers from "@/components/quiz/hooks/useQuizAnswers.ts";
import useQuizNavigation from "@/components/quiz/hooks/useQuizNavigation.ts";


const UseQuiz = () => {
    const [questions, setQuestions] = useState<Question[] | null>(db.questions as Question[]);
    const [loading, setLoading] = useState<boolean>(false);


    const params = useParams();

    const loadQuestionData = () => {
        const questionId = params?.questionId;
        if (!questionId) return setLoading(false);

        const questions = db.questions as unknown as Question[];

        setQuestions(questions)
        setLoading(false);
    }


    useEffect(() => {
        loadQuestionData()
    }, []);


    const {setAnswer, persistedCurrentQuestion, answers, clearAnswers} = useQuizAnswers();

    const {
        currentQuestion,
        handleBack,
        handleNext,
        handleRetake,
        questionPosition
    } = useQuizNavigation(questions, clearAnswers)

    const {translate} = useLocale(answers);

    const isFirst = questionPosition === 0;
    const isLast = questionPosition === Number(questions?.length) - 3;

    return {
        isFirst,
        isLast,
        currentQuestion,
        answers,
        persistedCurrentQuestion,
        translate,
        loading,
        questionPosition,
        questionsCount: Number(questions?.length) - 2,
        handleBack,
        handleNext,
        handleRetake,
        setAnswer
    }
};

export default UseQuiz;