import {useCallback, useEffect, useState} from "react";
import {LanguageQuestionWithAnswer, Locale, QuestionWithAnswer} from "@/types/quiz.ts";

const useLocale = (answers: QuestionWithAnswer[] | null) => {
    const [locale, setLocale] = useState<Locale>("en");

    const translate = useCallback((obj: Record<Locale, string>): string | undefined => {
        return obj?.[locale];
    }, [locale])


    const getLanguageAnswer = () => {
        const languageQuestionWithAnswer = answers
            ?.find(answer => answer.type === 'language') as LanguageQuestionWithAnswer | undefined;

        if (!languageQuestionWithAnswer) return "en" as Locale

        return languageQuestionWithAnswer.answer;
    }

    useEffect(() => {
        const locale = getLanguageAnswer();
        setLocale(locale)
    }, [answers]);

    return {
        locale,
        translate,
    }
};

export default useLocale;