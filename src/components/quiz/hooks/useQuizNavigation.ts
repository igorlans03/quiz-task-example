import {useEffect, useState} from "react";
import {Question} from "@/types/quiz.ts";
import {useNavigate} from "react-router-dom";

const useQuizNavigation = (questions: Question[] | null, clearAnswers: () => void) => {
    const [questionPosition, setQuestionPosition] = useState<number>(0);
    const currentQuestion = questions?.[questionPosition];

    const navigate = useNavigate();

    const handleNext = () => {
        if (!questions) return;

        const nextIndex = questionPosition + 1;

        if (nextIndex === questions.length) {
            window.localStorage.setItem('lastQuestionId', 'thank-you')
            navigate('/thank-you');
        }
        window.localStorage.setItem('lastQuestionId', questions?.[nextIndex].id)
        setQuestionPosition(nextIndex)
    }



    const handleBack = () => {
        if (!questions) return;

        const previousIndex = questionPosition - 1;

        if (previousIndex < 0) return;
        window.localStorage.setItem('lastQuestionId', questions?.[previousIndex].id)
        setQuestionPosition(previousIndex)
    }

    const handleRetake = () => {
        setQuestionPosition(0)
        clearAnswers();
    }


    const navigateToLastPassedQuestionId = () => {
        const lsLastQuestionId = window.localStorage.getItem('lastQuestionId');

        if (!lsLastQuestionId) {
            return setQuestionPosition(0);
        }

        const indexOfLast = questions?.findIndex(item => item.id === lsLastQuestionId);

        if (!indexOfLast || indexOfLast <= 0) {
            return setQuestionPosition(0)
        }

        setQuestionPosition(indexOfLast)
    }

    useEffect(() => {
        const id = currentQuestion?.id;
        if (!id) return;

        navigate(`/${id}`);

    }, [questionPosition]);


    useEffect(() => {
        navigateToLastPassedQuestionId();
    }, []);

    return {
        questionPosition,
        handleNext,
        handleBack,
        handleRetake,
        currentQuestion,
    }
};

export default useQuizNavigation;