import {MultipleQuestion} from "@/types/quiz.ts";
import {FC, useEffect, useState} from "react";
import Quiz from "@/components/quiz/quiz.tsx";
import {useQuizContext} from "@/components/quiz/context.ts";
import MultipleSelect from "@/components/common/multiple-select/multiple-select.tsx";
import staticTranslations from "@/components/quiz/data/translations.json";
import Button from "@/components/common/ui/button/button.tsx";

type MultipleSelectScreenScreenProps = {
    question: MultipleQuestion
}

const MultipleSelectScreen: FC<MultipleSelectScreenScreenProps> = ({question}) => {

    const [multipleAnswer, setMultipleAnswer] = useState<string[]>([]);


    const {handleNext, setAnswer, persistedCurrentQuestion, translate} = useQuizContext();

    useEffect(() => {
        if (persistedCurrentQuestion && persistedCurrentQuestion.id === question.id) {
            setMultipleAnswer(persistedCurrentQuestion.answer as string[])
        }
    }, [persistedCurrentQuestion]);


    const handleSubmit = () => {
        setAnswer({...question, answer: multipleAnswer as string[]})
        handleNext()
    }

    return (
        <>
            <Quiz.Header />
            <Quiz.Content>
                <Quiz.Info>
                    <Quiz.Title>{translate(question.title)}</Quiz.Title>
                </Quiz.Info>
                <Quiz.Form>
                    <MultipleSelect
                        options={question.options.map(item => ({...item, label: translate(item.label)}))}
                        value={multipleAnswer}
                        defaultValue={[]}
                        onValueChange={(newValue) => setMultipleAnswer(newValue)}
                    />

                    <Button onClick={handleSubmit} disabled={!multipleAnswer.length} text={translate(staticTranslations['nextButton']) || "Next"}/>
                </Quiz.Form>
            </Quiz.Content>

        </>

    );
};

export default MultipleSelectScreen;