import LoaderCircle from "@/components/common/loader-circle/loader-circle.tsx";
import {FC} from "react";
import styles from './loader-screen.module.scss';
import {useQuizContext} from "@/components/quiz/context.ts";
import staticTranslations from "@/components/quiz/data/translations.json";


type LoaderScreenProps = {
    setIsLoaderVisible: (newValue: boolean) => void;
}
const LoaderScreen: FC<LoaderScreenProps> = ({setIsLoaderVisible}) => {

    const {translate} = useQuizContext();

    const onComplete = () => {
        setIsLoaderVisible(false)
    }

    return (
        <div className={styles.loaderScreen}>
            <LoaderCircle timeout={5} onComplete={onComplete}/>
            <span>{translate(staticTranslations['findingCollections'] || "Finding collections for you...")}</span>
        </div>
    );
};

export default LoaderScreen;