import {BubbleQuestion, BubbleQuestionAnswer} from "@/types/quiz.ts";
import {FC, useEffect, useState} from "react";
import Quiz from "@/components/quiz/quiz.tsx";
import {useQuizContext} from "@/components/quiz/context.ts";
import BubbleMultipleSelect from "@/components/common/bubble-multiple-select/bubble-multiple-select.tsx";
import staticTranslations from '@/components/quiz/data/translations.json';
import Button from "@/components/common/ui/button/button.tsx";

type BubbleScreenScreenProps = {
    question: BubbleQuestion
}

const BubbleScreen: FC<BubbleScreenScreenProps> = ({question}) => {

    const [multipleAnswer, setMultipleAnswer] = useState<BubbleQuestionAnswer>([]);


    const {handleNext, setAnswer, persistedCurrentQuestion, translate} = useQuizContext();

    useEffect(() => {
        if (persistedCurrentQuestion && persistedCurrentQuestion.id === question.id) {
            setMultipleAnswer(persistedCurrentQuestion.answer as string[])
        }
    }, [persistedCurrentQuestion]);


    const handleSubmit = () => {
        setAnswer({...question, answer: multipleAnswer as BubbleQuestionAnswer})
        handleNext()
    }

    const disabled = multipleAnswer.length < 1 || multipleAnswer.length > 3

    return (
        <>
            <Quiz.Header/>
            <Quiz.Content>
                <Quiz.Info>
                    <Quiz.Title>{translate(question.title)}</Quiz.Title>
                    <Quiz.Description>{translate(question.description)}</Quiz.Description>
                </Quiz.Info>
                <Quiz.Form>
                    <BubbleMultipleSelect
                        options={question.options.map(item => ({...item, label: translate(item.label)}))}
                        value={multipleAnswer}
                        defaultValue={[]}
                        onValueChange={(newValue) => setMultipleAnswer(newValue)}
                    />

                    <Button onClick={handleSubmit} disabled={disabled} text={translate(staticTranslations['nextButton']) || "Next"}/>
                </Quiz.Form>
            </Quiz.Content>

        </>

    );
};

export default BubbleScreen;