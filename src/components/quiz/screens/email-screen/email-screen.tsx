import {EmailQuestion} from "@/types/quiz.ts";
import {FC, useEffect, useState} from "react";
import {useQuizContext} from "@/components/quiz/context.ts";
import Input from "@/components/common/ui/input/input.tsx";
import Quiz from "@/components/quiz/quiz.tsx";
import staticTranslations from "@/components/quiz/data/translations.json";
import LoaderScreen from "@/components/quiz/screens/loader-screen/loader-screen.tsx";
import Button from "@/components/common/ui/button/button.tsx";

type SingleSelectScreenProps = {
    question: EmailQuestion
}

const EmailScreen: FC<SingleSelectScreenProps> = ({question}) => {

    const [emailAnswer, setEmailAnswer] = useState<string>("");
    const [isValidEmail, setIsValidEmail] = useState(true);
    const [isLoaderVisible, setIsLoaderVisible] = useState(true);
    const handleEmailChange = (enteredEmail: string) => {
        setEmailAnswer(enteredEmail);

        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

        setIsValidEmail(emailRegex.test(enteredEmail));
    };

    const {handleNext, setAnswer, persistedCurrentQuestion, translate} = useQuizContext();

    useEffect(() => {
        if (persistedCurrentQuestion && persistedCurrentQuestion.id === question.id) {
            setEmailAnswer(persistedCurrentQuestion.answer as string)
        }
    }, [persistedCurrentQuestion]);


    const handleSubmit = () => {
        setAnswer({...question, answer: emailAnswer as string})
        handleNext()
    }


    if (isLoaderVisible) return <LoaderScreen setIsLoaderVisible={setIsLoaderVisible} />

    return (
        <>
            <Quiz.Content>
                <Quiz.Info>
                    <Quiz.Title>Email</Quiz.Title>
                    <Quiz.Description>{translate(staticTranslations['emailGetFullAccess']) || "Enter your email to get full access"}</Quiz.Description>
                </Quiz.Info>
                <Quiz.Form>
                        <Input
                            style={{borderColor: !isValidEmail ? "red" : undefined}}
                            placeholder={translate(staticTranslations['yourEmail']) || "Your email"}
                            value={emailAnswer as string}
                            onValueChange={(newValue) => handleEmailChange(newValue)}
                        />

                        {!isValidEmail && <p style={{color: 'red'}}>Please enter a valid email address.</p>}

                    <Button onClick={handleSubmit} disabled={!isValidEmail || !emailAnswer}
                                  text={translate(staticTranslations['nextButton']) || "Next"}/>
                </Quiz.Form>
            </Quiz.Content>

        </>

    );
};

export default EmailScreen;