import {LanguageQuestion, Locale} from "@/types/quiz.ts";
import {FC, useEffect, useState} from "react";
import SingleSelect from "@/components/common/single-select/single-select.tsx";
import Quiz from "@/components/quiz/quiz.tsx";
import {useQuizContext} from "@/components/quiz/context.ts";
import staticTranslations from "@/components/quiz/data/translations.json";
import Button from "@/components/common/ui/button/button.tsx";

type LanguageScreenProps = {
    question: LanguageQuestion
}

const LanguageScreen: FC<LanguageScreenProps> = ({question}) => {
    const {handleNext, setAnswer, persistedCurrentQuestion, translate} = useQuizContext();

    const [languageAnswer, setLanguageAnswer] = useState<Locale | null>(null);


    useEffect(() => {
        if (persistedCurrentQuestion && persistedCurrentQuestion.id === question.id) {
            setLanguageAnswer(persistedCurrentQuestion.answer as Locale)

        }
    }, [persistedCurrentQuestion]);

    const handleSubmit = () => {
        setAnswer({...question, answer: languageAnswer as Locale})
        handleNext()
    }


    return (
        <>
            <Quiz.Header/>
            <Quiz.Content>
                <Quiz.Info>
                    <Quiz.Title>{translate(question.title)}</Quiz.Title>
                    <Quiz.Description>{translate(question.description)}</Quiz.Description>
                </Quiz.Info>

                <Quiz.Form>
                    <SingleSelect
                        options={question.options.map(item => ({...item, label: translate(item.label)}))}
                        value={languageAnswer as string}
                        onValueChange={(newValue) => setLanguageAnswer(newValue as Locale)}
                    />
                    <Button disabled={!languageAnswer} onClick={handleSubmit} text={translate(staticTranslations['nextButton']) || "Next"}/>
                </Quiz.Form>

            </Quiz.Content>

        </>

    );
};

export default LanguageScreen;