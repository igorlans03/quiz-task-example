import {SingleQuestion} from "@/types/quiz.ts";
import {FC, useEffect, useState} from "react";
import SingleSelect from "@/components/common/single-select/single-select.tsx";
import Quiz from "@/components/quiz/quiz.tsx";
import {useQuizContext} from "@/components/quiz/context.ts";
import staticTranslations from "@/components/quiz/data/translations.json";
import Button from "@/components/common/ui/button/button.tsx";

type SingleSelectScreenProps = {
    question: SingleQuestion
}

const SingleSelectScreen: FC<SingleSelectScreenProps> = ({question}) => {

    const [singleAnswer, setSingleAnswer] = useState<string | null>(null);


    const {handleNext, setAnswer, persistedCurrentQuestion, translate} = useQuizContext();

    useEffect(() => {
        if (persistedCurrentQuestion && persistedCurrentQuestion.id === question.id) {
            setSingleAnswer(persistedCurrentQuestion.answer as string)
        } else {
            setSingleAnswer(null);
        }
    }, [persistedCurrentQuestion, question]);


    const handleSubmit = () => {
        setAnswer({...question, answer: singleAnswer as string})
        handleNext()
    }


    return (
        <>

            <Quiz.Header/>
            <Quiz.Content>
                <Quiz.Info>
                    <Quiz.Title>{translate(question.title)}</Quiz.Title>
                </Quiz.Info>
                <Quiz.Form>
                    <SingleSelect
                        options={question.options.map(item => ({...item, label: translate(item.label)}))}
                        value={singleAnswer as string}
                        onValueChange={(newValue) => setSingleAnswer(newValue)}
                    />
                    <Button onClick={handleSubmit} disabled={!singleAnswer} text={translate(staticTranslations['nextButton']) || "Next"}/>
                </Quiz.Form>

            </Quiz.Content>

        </>

    );
};

export default SingleSelectScreen;