import {useQuizContext} from "@/components/quiz/context.ts";
import generateCsv from "@/components/quiz/utils/generateCsv.ts";
import Quiz from "@/components/quiz/quiz.tsx";
import staticTranslations from "@/components/quiz/data/translations.json";
import Button from "@/components/common/ui/button/button.tsx";

const ThankYouScreen = () => {

    const {handleRetake, answers, translate} = useQuizContext();


    const downloadCsv = () => {
        generateCsv(answers);
    }


    return (
        <>
            <Quiz.Content>
                <Quiz.Info>
                    <Quiz.Title>{translate(staticTranslations['thankYou']) || "Thank you"}</Quiz.Title>
                    <Quiz.Description>{translate(staticTranslations['forSupport']) || "for supporting us and passing the quiz"}</Quiz.Description>
                </Quiz.Info>
                <Quiz.Form>
                    <Button onClick={downloadCsv} text={translate(staticTranslations['downloadButton']) || "Download my answers"}/>
                    <Button onClick={handleRetake} text={translate(staticTranslations['retakeButton']) || 'Retake quiz'}/>
                </Quiz.Form>
            </Quiz.Content>


        </>
    );
};

export default ThankYouScreen;