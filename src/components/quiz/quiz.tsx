import {FC, ReactNode} from "react";
import {QuizContext} from "@/components/quiz/context.ts";
import useQuiz from "@/components/quiz/hooks/useQuiz.ts";
import Header from "@/components/quiz/modules/header/header.tsx";
import Content from "@/components/quiz/modules/content/content.tsx";
import Title from "@/components/quiz/modules/content/info/title/title.tsx";
import Description from "@/components/quiz/modules/content/info/description/description.tsx";
import Info from "@/components/quiz/modules/content/info/info.tsx";
import Form from "@/components/quiz/modules/content/form/form.tsx";


type QuizComposition = {
    Header: typeof Header;
    Content: typeof Content;
    Title: typeof Title;
    Description: typeof Description;
    Info: typeof Info;
    Form: typeof Form;
}

type QuizProps = {
    children: ReactNode | ReactNode[]
}
const Quiz: FC<QuizProps> & QuizComposition = ({children}) => {
    const values = useQuiz();

    return (
        <QuizContext.Provider value={values}>
            {children}
        </QuizContext.Provider>
    );
};

Quiz.Header = Header;
Quiz.Description = Description;
Quiz.Title = Title;
Quiz.Content = Content;
Quiz.Info = Info;
Quiz.Form = Form;

export default Quiz;