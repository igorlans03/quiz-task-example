import {Outlet, Route, Routes} from "react-router-dom";
import QuestionPage from "@/pages/question.page.tsx";
import Quiz from "@/components/quiz/quiz.tsx";
import ThankYouPage from "@/pages/thank-you.page.tsx";
import MainPage from "@/pages/main.page.tsx";

function App() {


    return (
        <Routes>
            <Route element={<Quiz><Outlet/></Quiz>}>
                <Route index path={'/'} element={<MainPage/>}/>
                <Route path={':questionId'} element={<QuestionPage/>}/>
                <Route path={'/thank-you'} element={<ThankYouPage/>}/>
            </Route>

        </Routes>
    )
}

export default App
