import Container from "@/components/common/container/container.tsx";
import ThankYouScreen from "@/components/quiz/screens/thank-you-screen/thank-you-screen.tsx";

const ThankYouPage = () => {


    return (
        <Container>
           <ThankYouScreen />
        </Container>
    );
};

export default ThankYouPage;