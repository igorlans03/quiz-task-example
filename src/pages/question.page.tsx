import LanguageScreen from "@/components/quiz/screens/language-screen/language-screen.tsx";
import {BubbleQuestion, EmailQuestion, LanguageQuestion, MultipleQuestion, SingleQuestion} from "@/types/quiz.ts";
import Container from "@/components/common/container/container.tsx";
import {useQuizContext} from "@/components/quiz/context.ts";
import SingleSelectScreen from "@/components/quiz/screens/single-select-screen/single-select-screen.tsx";
import MultipleSelectScreen from "@/components/quiz/screens/multiple-select-screen/multiple-select-screen.tsx";
import BubbleScreen from "@/components/quiz/screens/bubble-screen/bubble-screen.tsx";
import EmailScreen from "@/components/quiz/screens/email-screen/email-screen.tsx";

const QuestionPage = ({}) => {
    const {currentQuestion, loading} = useQuizContext();

    const currentQuestionType = currentQuestion?.type;


    const renderQuestion = () => {
        switch (currentQuestionType) {
            case 'bubble':
                return <BubbleScreen question={currentQuestion as BubbleQuestion} />
            case 'single':
                return <SingleSelectScreen question={currentQuestion as SingleQuestion} />
            case 'multiple':
                return <MultipleSelectScreen question={currentQuestion as MultipleQuestion} />
            case 'email':
                return <EmailScreen question={currentQuestion as EmailQuestion} />
            case 'language':
                return <LanguageScreen question={currentQuestion as LanguageQuestion} />
            default:
                return <div>Something went wrong!</div>
        }
    }

    if (loading) return null;

    return (
        <Container>
            {renderQuestion()}
        </Container>
    );
};

export default QuestionPage;