export interface IColorsSCSS {
    bgColor: string;
    primaryColor: string;
    secondaryColor: string;
    primaryColorDark: string;
    primaryColorLight: string;
    primaryColorOpacity20: string;
    grey: string;
    textColor: string;
    textColorDark: string;
    textColorLight: string;
    textColorLightOpacity50: string;
}

export const SCSSVariables: IColorsSCSS;

export default SCSSVariables;