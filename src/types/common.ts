import {ComponentPropsWithRef, ReactNode} from "react";
import {ObjectWithTranslations} from "@/types/quiz.ts";

export type HTMLDivProps = ComponentPropsWithRef<'div'>;

export type WrapperDivProps = {
    children: ReactNode | ReactNode[]
} & HTMLDivProps;


export type UntranslatedSelectOption = {
    label: ObjectWithTranslations;
    value: string;
}

export type SelectOption = {
    label: string | ReactNode;
    value: string;
}