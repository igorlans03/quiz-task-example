import {UntranslatedSelectOption} from "@/types/common.ts";

export type Locale = 'en' | 'fr' | 'de' | 'es';

export type ObjectWithTranslations = Record<Locale, string>;


export type LanguageQuestion = {
    id: "language";
    type: 'language';
    title: ObjectWithTranslations;
    description: ObjectWithTranslations;
    options: UntranslatedSelectOption[];
}

type WithAnswer<Question, Answer> = Question & {
    answer: Answer
}

export type LanguageAnswer = Locale;

export type LanguageQuestionWithAnswer = WithAnswer<LanguageQuestion, LanguageAnswer>;


export type MultipleQuestion = {
    id: string;
    type: 'multiple';
    title: ObjectWithTranslations;
    options: UntranslatedSelectOption[];
}

export type MultipleQuestionAnswer = string[];


export type MultipleQuestionWithAnswer = WithAnswer<MultipleQuestion, MultipleQuestionAnswer>

export type SingleQuestion = {
    id: string;
    type: 'single';
    title: ObjectWithTranslations;
    options: UntranslatedSelectOption[];
}

export type SingleQuestionAnswer = string;


export type SingleQuestionWithAnswer = WithAnswer<SingleQuestion, SingleQuestionAnswer>


export type BubbleQuestion = {
    id: string;
    type: 'bubble';
    title: ObjectWithTranslations;
    description: ObjectWithTranslations;
    options: UntranslatedSelectOption[];
}

export type BubbleQuestionAnswer = string[];


export type BubbleQuestionWithAnswer = WithAnswer<BubbleQuestion, BubbleQuestionAnswer>

export type EmailQuestion = {
    id: "email";
    type: 'email';
    title: "Email";
}

export type EmailQuestionAnswer = string;


export type EmailQuestionWithAnswer = WithAnswer<EmailQuestion, EmailQuestionAnswer>


export type Question = LanguageQuestion
    | MultipleQuestion
    | SingleQuestion
    | BubbleQuestion
    | EmailQuestion


export type QuestionWithAnswer = LanguageQuestionWithAnswer
    | MultipleQuestionWithAnswer
    | SingleQuestionWithAnswer
    | BubbleQuestionWithAnswer
    | EmailQuestionWithAnswer
